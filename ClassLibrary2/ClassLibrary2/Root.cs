﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class Root : Vegetable
    {
        private double _peelWeight = 0;
        public Root()
        {
            vegetableType = 3;
        }
        public double PeelWeight
        {
            get { return _peelWeight; }
            set
            {
                _peelWeight = value;
                Weight = Weight - _peelWeight;
            }
        }
    }
}
