﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Vegetables;

namespace WindowsFormsApplication1
{
    public class Data
    {
        private string _name;
        private double _weight;
        private double _calorificOfHundredGrams;
        private int _weightOfInedibleLeaves;
        private double _peelWeight;
        private string _prepared;

        public int _sort;
        public int _sortCabbage = 1;
        public int _sortCarrot = 2;
        public int _sortRoot = 3;

        public Data() { }

        public Data(string name, double weight, double calorificofHundredGrams, int weightOfInedibleLeaves)
        {
            _sort = _sortCabbage;
            _name = name;
            _weight = weight;
            _calorificOfHundredGrams = calorificofHundredGrams;
            _weightOfInedibleLeaves = weightOfInedibleLeaves;
        }

        public Data(string name, double weight, double calorificofHundredGrams, double peelWeight)
        {
            _sort = _sortRoot;
            _name = name;
            _weight = weight;
            _calorificOfHundredGrams = calorificofHundredGrams;
            _peelWeight = peelWeight;
        }

        public Data(string name, double weight, double calorificOfHundredGrams, double peelWeight, string prepared)
        {
            _sort = _sortCarrot;
            _name = name;
            _weight = weight;
            _calorificOfHundredGrams = calorificOfHundredGrams;
            _peelWeight = peelWeight;
            _prepared = prepared;
        }

        public Data(Vegetable vegetable) 
        {
            if (vegetable.vegetableType == _sortCabbage)
            {
                Cabbage cabbage = (Cabbage)vegetable;
                _sort = _sortCabbage;
                _name = cabbage.Name;
                _calorificOfHundredGrams = cabbage.CalorificOfHundredGrams;
                _weight = cabbage.Weight;
                _weightOfInedibleLeaves = cabbage.WeightOfInedibleLeaves;
            }
            if (vegetable.vegetableType == _sortRoot)
            {
                Root root = (Root)vegetable;
                _sort = _sortRoot;
                _name = root.Name;
                _calorificOfHundredGrams = root.CalorificOfHundredGrams;
                _weight = root.Weight;
                _peelWeight = root.PeelWeight;
            }
            if (vegetable.vegetableType == _sortCarrot)
            {
                Carrot carrot = (Carrot)vegetable;
                _sort = _sortCarrot;
                _name = carrot.Name;
                _calorificOfHundredGrams = carrot.CalorificOfHundredGrams;
                _weight = carrot.Weight;
                _peelWeight = carrot.PeelWeight;
                _prepared = carrot.GetPrepared().ToString();
            }
        }

        public void MoveToFile()
        {
            StreamWriter line = new StreamWriter("TextFile2.txt");
            line.WriteLine(_sort);
            line.WriteLine(_name);
            line.WriteLine(_weight);
            line.WriteLine(_calorificOfHundredGrams);
            if (_sort == _sortCabbage)
                line.WriteLine(_weightOfInedibleLeaves);
            if (_sort == _sortRoot)
                line.WriteLine(_peelWeight);
            if (_sort == _sortCarrot)
            {
                line.WriteLine(_prepared);
                line.WriteLine(_peelWeight);
            }
            line.Close();
        }

        public void ReadFromFile()
        {
            StreamReader line = new StreamReader("TextFile2.txt");
            _sort = Int32.Parse(line.ReadLine());
            _name = line.ReadLine();
            _weight = Double.Parse(line.ReadLine());
            _calorificOfHundredGrams = Double.Parse(line.ReadLine());
            if (_sort == _sortCabbage)
                _weightOfInedibleLeaves = Int32.Parse(line.ReadLine());
            if (_sort == _sortRoot)
                _peelWeight = Double.Parse(line.ReadLine());
            if (_sort == _sortCarrot)
            {
                _prepared = line.ReadLine();
                _peelWeight = Double.Parse(line.ReadLine());
            }
            line.Close();
        }
        public Vegetable ReturnVegetable()
        {
            if (_sort == _sortCabbage)
            {
                Cabbage vegetable = new Cabbage();
                vegetable.Name = _name;
                vegetable.CalorificOfHundredGrams = _calorificOfHundredGrams;
                vegetable.Weight = _weight;
                vegetable.WeightOfInedibleLeaves = _weightOfInedibleLeaves;
                vegetable.CountCalorific();
                return vegetable;
            }
            if (_sort == _sortRoot)
            {
                Root vegetable = new Root();
                vegetable.Name = _name;
                vegetable.CalorificOfHundredGrams = _calorificOfHundredGrams;
                vegetable.Weight = _weight;
                vegetable.PeelWeight = _peelWeight;
                vegetable.CountCalorific();
                return vegetable;
            }
            if (_sort == _sortCarrot)
            {
                Carrot vegetable = new Carrot();
                vegetable.Name = _name;
                vegetable.CalorificOfHundredGrams = _calorificOfHundredGrams;
                vegetable.Weight = _weight;
                vegetable.PeelWeight = _peelWeight;
                vegetable.SetPrepared(_prepared);
                vegetable.CountCalorific();
                return vegetable;
            }
            else { return null; }
        }

        public string GetDataName()
        {
            return _name;
        }

        public double GetDataWeight()
        {
            if (_sort == _sortCabbage) _weight += _weightOfInedibleLeaves;
            if ((_sort == _sortRoot) || (_sort == _sortCarrot)) _weight += _peelWeight;
            return _weight;
        }

        public double GetDataCalorificOfHundredGrams()
        {
            if (_sort == _sortCarrot)  
                _calorificOfHundredGrams -= Double.Parse(_prepared);          
             return _calorificOfHundredGrams;
        }

        public double GetDataPeelWeight()
        {
            return _peelWeight;
        }

        public double GetDataWeightOfInedibleLeaves()
        {
            return _weightOfInedibleLeaves;
        }

        public string GetDataPrepared()
        {
            return _prepared;
        }

        public int GetDataSort()
        {
            return _sort;
        }
    }
}
