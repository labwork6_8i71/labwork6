﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassLibrary3
{
    public partial class AddForm : Form
    {
        private const int _buttonWidth = 30;
        private const int _buttonLength = 80;

        public int _sort = 0;
        public int _sortCabbage = 1;
        public int _sortCarrot = 2;
        public int _sortRoot = 3;

        public Data data;

        private Button _buttonOk;
        private Button _buttonCancel;
        private Label _labelClass;
        private Label _labelCalorificOfHundredGrams;
        private Label _labelName;
        private Label _labelWeight;
        private Label _labelWeightOfInedibleLeaves;
        private Label _labelPeelWeight;
        private Label _labelPrepared;

        private RadioButton _radioButtonCabbage;
        private RadioButton _radioButtonRoot;
        private RadioButton _radioButtonCarrot;

        public TextBox _textBoxCalorificOfHundredGrams;
        public TextBox _textBoxName;
        public TextBox _textBoxWeight;
        public TextBox _textBoxWeightOfInedibleLeaves;
        public TextBox _textBoxPeelWeight;
        public TextBox _textBoxPrepared;

        public delegate void MethodContainer();
        public event MethodContainer OKButtonClicked;

        public AddForm()
        {
            InitializeComponent();

            this.Text = "AddForm";
            this.Width = 400;
            this.Height = 400;

            _buttonOk = new Button();
            _buttonOk.Text = "OK";
            _buttonOk.Parent = this;
            _buttonOk.Location = new Point(100, this.Height - 80);
            _buttonOk.Size = new Size(_buttonLength, _buttonWidth);
            _buttonOk.Click += _buttonOk_Click;

            _buttonCancel = new Button();
            _buttonCancel.Text = "Cancel";
            _buttonCancel.Parent = this;
            _buttonCancel.Location = new Point(110 + _buttonLength, this.Height - 80);
            _buttonCancel.Size = new Size(_buttonLength, _buttonWidth);
            _buttonCancel.Click += _buttonCancel_Click;

            _radioButtonCabbage = new RadioButton();
            _radioButtonCabbage.Parent = this;
            _radioButtonCabbage.Text = "Cabbage";
            _radioButtonCabbage.Location = new Point(200, 10);
            _radioButtonCabbage.CheckedChanged += _radioButtonCabbage_CheckedChanged;

            _radioButtonCarrot = new RadioButton();
            _radioButtonCarrot.Parent = this;
            _radioButtonCarrot.Text = "Carrot";
            _radioButtonCarrot.Location = new Point(200, 30);
            _radioButtonCarrot.CheckedChanged += _radioButtonCarrot_CheckedChanged;

            _radioButtonRoot = new RadioButton();
            _radioButtonRoot.Parent = this;
            _radioButtonRoot.Text = "Root";
            _radioButtonRoot.Location = new Point(200, 50);
            _radioButtonRoot.CheckedChanged += _radioButtonRoot_CheckedChanged;

            _labelClass = new Label();
            _labelClass.Text = "Class:";
            _labelClass.Parent = this;
            _labelClass.Location = new Point(50, 10);

            _labelName = new Label();
            _labelName.Text = "Name:";
            _labelName.Parent = this;
            _labelName.Location = new Point(50, 80);

            _labelCalorificOfHundredGrams = new Label();
            _labelCalorificOfHundredGrams.Text = "CalorificOfHundredGrams:";
            _labelCalorificOfHundredGrams.Parent = this;
            _labelCalorificOfHundredGrams.Location = new Point(50, 110);
            _labelCalorificOfHundredGrams.AutoSize = true;

            _labelWeight = new Label();
            _labelWeight.Text = "Weight:";
            _labelWeight.Parent = this;
            _labelWeight.Location = new Point(50, 140);

            _textBoxName = new TextBox();
            _textBoxName.Parent = this;
            _textBoxName.Location = new Point(200, 80);

            _textBoxCalorificOfHundredGrams = new TextBox();
            _textBoxCalorificOfHundredGrams.Parent = this;
            _textBoxCalorificOfHundredGrams.Location = new Point(200, 110);

            _textBoxWeight = new TextBox();
            _textBoxWeight.Parent = this;
            _textBoxWeight.Location = new Point(200, 140);
        }

        private void _Clean()
        {
            if (_labelPeelWeight != null) _labelPeelWeight.Hide();
            if (_textBoxPeelWeight != null) _textBoxPeelWeight.Hide();

            if (_labelPrepared != null) _labelPrepared.Hide();
            if (_textBoxPrepared != null) _textBoxPrepared.Hide();

            if (_labelWeightOfInedibleLeaves != null) _labelWeightOfInedibleLeaves.Hide();
            if (_textBoxWeightOfInedibleLeaves != null) _textBoxWeightOfInedibleLeaves.Hide();

        }

        private void _radioButtonCabbage_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonCabbage.Checked)
            {
                _sort = _sortCabbage;
                _Clean();

                _labelWeightOfInedibleLeaves = new Label();
                _labelWeightOfInedibleLeaves.Text = "WeightOfInedibleLeaves:";
                _labelWeightOfInedibleLeaves.Parent = this;
                _labelWeightOfInedibleLeaves.Location = new Point(50, 170);
                _labelWeightOfInedibleLeaves.AutoSize = true;

                _textBoxWeightOfInedibleLeaves = new TextBox();
                _textBoxWeightOfInedibleLeaves.Parent = this;
                _textBoxWeightOfInedibleLeaves.Location = new Point(200, 170);
            }
        }

        private void _radioButtonRoot_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonRoot.Checked)
            {
                _sort = _sortRoot;
                _Clean();

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);
            }
        }

        private void _radioButtonCarrot_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonCarrot.Checked)
            {
                _sort = _sortCarrot;
                _Clean();

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);

                _labelPrepared = new Label();
                _labelPrepared.Text = "Prepared";
                _labelPrepared.Parent = this;
                _labelPrepared.Location = new Point(50, 200);

                _textBoxPrepared = new TextBox();
                _textBoxPrepared.Parent = this;
                _textBoxPrepared.Location = new Point(200, 200);
            }
        }

        private void _buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _buttonOk_Click(object sender, EventArgs e)
        {
            if (_sort == _sortCabbage)
                data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                    Int32.Parse(_textBoxWeightOfInedibleLeaves.Text));
            if (_sort == _sortRoot)
                data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                    Double.Parse(_textBoxPeelWeight.Text));
            if (_sort == _sortCarrot)
                data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                    Double.Parse(_textBoxPeelWeight.Text), _textBoxPrepared.Text);
            data.MoveToFile();
            Close();
            OKButtonClicked();
        }
    }
}
