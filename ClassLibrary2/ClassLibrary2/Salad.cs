﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class Salad
    {
        public delegate void MethodContainer();
        public event MethodContainer TotalCalorificChanged;

        private int _vegetablesCount = 0;
        public double calorific;

        private List<Vegetable> _listOfVegetables = new List<Vegetable>();
        public Salad() { }
       
        public void AddVegetable(Vegetable vegetable)
        {
            _listOfVegetables.Add(vegetable);
            _vegetablesCount++;
            CalculateCalorific();
            TotalCalorificChanged();
        }

        public void AddVegetable(int index, Vegetable vegetable)
        {
            _listOfVegetables.Insert(index, vegetable);
            _vegetablesCount++;
            CalculateCalorific();
            TotalCalorificChanged();
        }

        public void DeleteVegetable()
        {
            _listOfVegetables.RemoveAt(_listOfVegetables.Count - 1);
            _vegetablesCount--;
            CalculateCalorific();
            TotalCalorificChanged();
        }

        public void DeleteVegetable(int index)
        {
            _listOfVegetables.RemoveAt(index);
            _vegetablesCount--;
            CalculateCalorific();
            TotalCalorificChanged();
        }
        public List<Vegetable> GetListOfVegetables()
        {
            return _listOfVegetables;
        }
        public double CalculateCalorific()  
        {
            calorific = 0;
            for (int i = 0; i < _listOfVegetables.Count; i++)
                calorific = calorific + (_listOfVegetables[i].Weight) * (_listOfVegetables[i].CalorificOfHundredGrams);           
            return calorific; 

        }

        public Vegetable GetCurrentVegetable(int index)
        {
            return _listOfVegetables[index];
        }
    }
}