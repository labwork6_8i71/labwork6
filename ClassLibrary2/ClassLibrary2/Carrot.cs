﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class Carrot : Root
    {
        private double _prepared = 0;

        public Carrot()
        {
            vegetableType = 2;
        }
        public double GetPrepared()
        {
            return _prepared;
        }
        public void SetPrepared(string prepared)
        {
            switch (prepared)
            {
                case "Boiled":
                    _prepared = -50; 
                    break;
                case "Fried":
                    _prepared = 100; 
                    break;
                case "Raw":
                    _prepared = 0; 
                    break;
                default:
                    break;
            }
            CalorificOfHundredGrams = CalorificOfHundredGrams + _prepared;
        }
    }
}