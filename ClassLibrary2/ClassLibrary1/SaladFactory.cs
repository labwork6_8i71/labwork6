﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class SaladFactory
    {
        public static Salad CreateSalad()
        {
            Carrot carrot = new Carrot();
            carrot.Name = "CarrotCarrot";
            carrot.Weight = 150;
            carrot.CalorificOfHundredGrams = 35;
            carrot.PeelWeight = 10;
            carrot.SetPrepared("Raw");

            Cabbage cabbage = new Cabbage();
            cabbage.Name = "CabbageCabbage";
            cabbage.Weight = 250;
            cabbage.CalorificOfHundredGrams = 16;
            cabbage.WeightOfInedibleLeaves = 45;

            Vegetable cucumber = new Vegetable();
            cucumber.Name = "Cucumber";
            cucumber.Weight = 100;
            cucumber.CalorificOfHundredGrams = 10;

            Salad salad = new Salad();
            salad.AddVegetable(carrot);
            salad.AddVegetable(cabbage);
            salad.AddVegetable(cucumber);

            salad.SetFlavouring("Sore cream");

            return salad;
        }
    }
}