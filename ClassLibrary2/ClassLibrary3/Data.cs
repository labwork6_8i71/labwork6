﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClassLibrary3
{
    public class Data
    {
        private string _name;
        private double _weight;
        private double _calorific;
        private int _weightOfInedibleLeaves;
        private double _peelWeight;
        private string _prepared;

        public int _sort;
        public int _sortCabbage = 1;
        public int _sortCarrot = 2;
        public int _sortRoot = 3;

        public Data() { }

        public Data(string name, double weight, double calorific, int weightOfInedibleLeaves)
        {
            this._sort = _sortCabbage;
            this._name = name;
            this._weight = weight;
            this._calorific = calorific;
            this._weightOfInedibleLeaves = weightOfInedibleLeaves;
        }

        public Data(string name, double weight, double calorific, double peelWeight)
        {
            this._sort = _sortRoot;
            this._name = name;
            this._weight = weight;
            this._calorific = calorific;
            this._peelWeight = peelWeight;
        }

        public Data(string name, double weight, double calorific, double peelWeight, string prepared)
        {
            this._sort = _sortCarrot;
            this._name = name;
            this._weight = weight;
            this._calorific = calorific;
            this._peelWeight = peelWeight;
            this._prepared = prepared;
        }

        public void MoveToFile()
        {
            StreamWriter line = new StreamWriter("TextFile2.txt");
            line.WriteLine(this._sort);
            line.WriteLine(this._name);
            line.WriteLine(this._weight);
            line.WriteLine(this._calorific);
            if (_sort == _sortCabbage)
                line.WriteLine(_weightOfInedibleLeaves);
            if (_sort == _sortRoot)
                line.WriteLine(this._peelWeight);
            if (_sort == _sortCarrot)
            {
                line.WriteLine(this._prepared);
                line.WriteLine(this._peelWeight);
            }
            line.Close();
        }

        public void ReadFromFile()
        {
            StreamReader line = new StreamReader("TextFile2.txt");
            _sort = Int32.Parse(line.ReadLine());
            _name = line.ReadLine();
            _weight = Double.Parse(line.ReadLine());
            _calorific = Double.Parse(line.ReadLine());
            if (_sort == _sortCabbage)
                _weightOfInedibleLeaves = Int32.Parse(line.ReadLine());
            if (_sort == _sortRoot)
                _peelWeight = Double.Parse(line.ReadLine());
            if (_sort == _sortCarrot)
            {
                _prepared = line.ReadLine();
                _peelWeight = Double.Parse(line.ReadLine());
            }
            line.Close();
        }
    }
}
