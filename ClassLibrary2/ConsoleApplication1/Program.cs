﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    class Program
    {
        static void Main(string[] args)
        {
            Salad salad = SaladFactory.CreateSalad();
            SaladPrinter saladPrinter = new SaladPrinter();
            saladPrinter.Print(salad);
            double totalCalorific = salad.CalculateCalorific();
            Console.WriteLine("Итоговая калорийность = " + totalCalorific + " ккал");
            Console.ReadKey();
        }
    }
}
