﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class SaladPrinter
    {
        public void Print(Salad salad)
        {
            List<Vegetable> _listOfVegetable = salad.GetListOfVegetables();
            double flauvoring = salad.GetFlavouring();
            for (int i = 0; i < _listOfVegetable.Count; i++)
            {
                Console.WriteLine("Название = " + _listOfVegetable[i].Name);
                Console.WriteLine("Вес = " + _listOfVegetable[i].Weight + " гр");
                Console.WriteLine("Калорийность на 100 гр = " + _listOfVegetable[i].CalorificOfHundredGrams + " ккал");
            }
            Console.WriteLine("Калорийность заправки = " + flauvoring + " ккал");
        }
    }
}

