﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class Vegetable
    {
        public int vegetableType;

        public string _name;
        private double _weight;
        public double _calorificOfHundredGrams;
        public double _calorific;
        public Vegetable() { }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public double CalorificOfHundredGrams
        {
            get { return _calorificOfHundredGrams; }
            set { _calorificOfHundredGrams = value; }
        }

        public void CountCalorific()
        {   
            _calorific = _calorificOfHundredGrams * _weight;
        }

        public string GetVegetableInfo()
        {
            return Name + " - " + (_calorific).ToString();
        }
    }
}
