﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vegetables;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form
    {
        private const int _buttonWidth = 30;
        private const int _buttonLength = 80;

        private Button _buttonAdd;
        private Button _buttonEdit;
        private Button _buttonDelete;
        private ListBox _listOfVegetables;
        private Label _labelTotalCalorific;

        private Salad _salad;

        private int addedIndex = 0;
        private int changedIndex = 0;

        public MainForm()
        {
            InitializeComponent();

            this.Width = 400;
            this.Height = 300;

            _buttonAdd = new Button();
            _buttonAdd.Text = "Add";
            _buttonAdd.Parent = this;
            _buttonAdd.Location = new Point(10, 10);
            _buttonAdd.Size = new Size(_buttonLength, _buttonWidth);
            _buttonAdd.Click += _buttonAdd_Click;


            _buttonEdit = new Button();
            _buttonEdit.Text = "Edit";
            _buttonEdit.Parent = this;
            _buttonEdit.Location = new Point(10, 2 * 10 + _buttonWidth);
            _buttonEdit.Size = new Size(_buttonLength, _buttonWidth);
            _buttonEdit.Click += _buttonEdit_Click;


            _buttonDelete = new Button();
            _buttonDelete.Text = "Delete";
            _buttonDelete.Parent = this;
            _buttonDelete.Location = new Point(10, 3 * 10 + 2 * _buttonWidth);
            _buttonDelete.Size = new Size(_buttonLength, _buttonWidth);
            _buttonDelete.Click += _buttonDelete_Click;


            _listOfVegetables = new ListBox();
            _listOfVegetables.Parent = this;
            _listOfVegetables.Width = 2 * _listOfVegetables.Width;
            _listOfVegetables.Location = new Point(this.Width - _listOfVegetables.Width - 30, 10);


            _labelTotalCalorific = new Label();
            _labelTotalCalorific.Text = "Total calorific, cal:";
            _labelTotalCalorific.Parent = this;
            _labelTotalCalorific.Location = new Point(40, this.Height - 100);
            _labelTotalCalorific.AutoSize = true;

            _salad = new Salad();
            _salad.TotalCalorificChanged += _salad_TotalCalorificChanged;
        }

        private void _buttonAdd_Click(object sender, EventArgs e)
        {
            AddForm AddForm = new AddForm();
            AddForm.OKButtonClickedAdd += AddForm_OkButtonClickedAdd;
            AddForm.ShowDialog();
        }

        private void AddForm_OkButtonClickedAdd()
        {
            Data data = new Data();
            data.ReadFromFile();
            AddEditDelete.AddToList(data.ReturnVegetable() , _listOfVegetables, addedIndex);
            AddEditDelete.AddToSalad(data.ReturnVegetable(), _salad);
            addedIndex++;
        }

        private void _buttonDelete_Click(object sender, EventArgs e)
        {
            if (_listOfVegetables.SelectedIndex == -1)
                MessageBox.Show("You have not selected any item!");
            else
            {
                int index = _listOfVegetables.SelectedIndex;
                AddEditDelete.DeleteFromList(_listOfVegetables, index);
                AddEditDelete.DeleteFromSalad(_salad, index);
                addedIndex = _listOfVegetables.Items.Count;
            }
        }

        private void _buttonEdit_Click(object sender, EventArgs e)
        {
            if (_listOfVegetables.SelectedIndex == -1)
                MessageBox.Show("You have not selected any item!");
            else
            {
                int index = _listOfVegetables.SelectedIndex;
                Data data = new Data(_salad.GetCurrentVegetable(index));
                AddForm window = new AddForm(data);
                window.OKButtonClickedEdit += Window_OKButtonEditClicked;
                changedIndex = index;
                window.ShowDialog();
            }
        }

        private void Window_OKButtonEditClicked()
        {
            Data holder = new Data();
            holder.ReadFromFile();
            AddEditDelete.ReplaceInList(holder.ReturnVegetable(), _listOfVegetables, changedIndex);
            AddEditDelete.ReplaceInSalad(holder.ReturnVegetable(), _salad, changedIndex);
        }

        private void _salad_TotalCalorificChanged()
        {
            _labelTotalCalorific.Text = "Total calorific, cal:" + _salad.CalculateCalorific();
        }
    }
}
