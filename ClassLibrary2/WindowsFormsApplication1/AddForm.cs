﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AddForm : Form
    {
        private const int _buttonWidth = 30;
        private const int _buttonLength = 80;

        public int _sort = 0;
        public int _sortCabbage = 1;
        public int _sortCarrot = 2;
        public int _sortRoot = 3;

        private int _windowType;
        private int _windowTypeAdd = 0;
        private int _windowTypeEdit = 1;

        public Data data;

        private Button _buttonOk;
        private Button _buttonCancel;
        private Label _labelClass;
        private Label _labelCalorificOfHundredGrams;
        private Label _labelName;
        private Label _labelWeight;
        private Label _labelWeightOfInedibleLeaves;
        private Label _labelPeelWeight;
        private Label _labelPrepared;
        private Label _selectedTypeLabel;

        private RadioButton _radioButtonCabbage;
        private RadioButton _radioButtonRoot;
        private RadioButton _radioButtonCarrot;

        public TextBox _textBoxCalorificOfHundredGrams;
        public TextBox _textBoxName;
        public TextBox _textBoxWeight;
        public TextBox _textBoxWeightOfInedibleLeaves;
        public TextBox _textBoxPeelWeight;
        public TextBox _textBoxPrepared;

        public delegate void MethodContainer();
        public event MethodContainer OKButtonClickedAdd;
        public event MethodContainer OKButtonClickedEdit;

        public AddForm()
        {
            _windowType = _windowTypeAdd;
            InitializeComponent();

            Text = "AddForm";
            Width = 400;
            Height = 350;

            _buttonOk = new Button();
            _buttonOk.Text = "OK";
            _buttonOk.Parent = this;
            _buttonOk.Location = new Point(100, Height - 80);
            _buttonOk.Size = new Size(_buttonLength, _buttonWidth);
            _buttonOk.Click += _buttonOk_Click;

            _buttonCancel = new Button();
            _buttonCancel.Text = "Cancel";
            _buttonCancel.Parent = this;
            _buttonCancel.Location = new Point(110 + _buttonLength, Height - 80);
            _buttonCancel.Size = new Size(_buttonLength, _buttonWidth);
            _buttonCancel.Click += _buttonCancel_Click;

            _radioButtonCabbage = new RadioButton();
            _radioButtonCabbage.Parent = this;
            _radioButtonCabbage.Text = "Cabbage";
            _radioButtonCabbage.Location = new Point(200, 10);
            _radioButtonCabbage.CheckedChanged += _radioButtonCabbage_CheckedChanged;

            _radioButtonCarrot = new RadioButton();
            _radioButtonCarrot.Parent = this;
            _radioButtonCarrot.Text = "Carrot";
            _radioButtonCarrot.Location = new Point(200, 30);
            _radioButtonCarrot.CheckedChanged += _radioButtonCarrot_CheckedChanged;

            _radioButtonRoot = new RadioButton();
            _radioButtonRoot.Parent = this;
            _radioButtonRoot.Text = "Root";
            _radioButtonRoot.Location = new Point(200, 50);
            _radioButtonRoot.CheckedChanged += _radioButtonRoot_CheckedChanged;

            _labelClass = new Label();
            _labelClass.Text = "Class:";
            _labelClass.Parent = this;
            _labelClass.Location = new Point(50, 10);

            _labelName = new Label();
            _labelName.Text = "Name:";
            _labelName.Parent = this;
            _labelName.Location = new Point(50, 80);

            _labelCalorificOfHundredGrams = new Label();
            _labelCalorificOfHundredGrams.Text = "CalorificOfHundredGrams:";
            _labelCalorificOfHundredGrams.Parent = this;
            _labelCalorificOfHundredGrams.Location = new Point(50, 110);
            _labelCalorificOfHundredGrams.AutoSize = true;

            _labelWeight = new Label();
            _labelWeight.Text = "Weight:"; // удалила размерность
            _labelWeight.Parent = this;
            _labelWeight.Location = new Point(50, 140);

            _textBoxName = new TextBox();
            _textBoxName.Parent = this;
            _textBoxName.Location = new Point(200, 80);

            _textBoxCalorificOfHundredGrams = new TextBox();
            _textBoxCalorificOfHundredGrams.Parent = this;
            _textBoxCalorificOfHundredGrams.Location = new Point(200, 110);
            _textBoxCalorificOfHundredGrams.KeyPress += _textBox_KeyPress;

            _textBoxWeight = new TextBox();
            _textBoxWeight.Parent = this;
            _textBoxWeight.Location = new Point(200, 140);
            _textBoxWeight.KeyPress += _textBox_KeyPress;
        }

        private void _textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) 
            {
                e.Handled = true;
            }
        }

        public AddForm(Data data)
        {
            Text = "EditForm";
            Width = 350;
            Height = 350;

            _windowType = _windowTypeEdit;
            InitializeComponent();

            _labelClass = new Label();
            _labelClass.Text = "Class:";
            _labelClass.Parent = this;
            _labelClass.Location = new Point(50, 30);

            _textBoxName = new TextBox();
            _textBoxName.Parent = this;
            _textBoxName.Location = new Point(200, 80);
            _textBoxName.Text = data.GetDataName();

            _labelName = new Label();
            _labelName.Parent = this;
            _labelName.Location = new Point(50, 80);
            _labelName.Text = "Name:";

            _labelCalorificOfHundredGrams = new Label();
            _labelCalorificOfHundredGrams.Text = "CalorificOfHundredGrams:";
            _labelCalorificOfHundredGrams.Parent = this;
            _labelCalorificOfHundredGrams.Location = new Point(50, 110);
            _labelCalorificOfHundredGrams.AutoSize = true;

            _labelWeight = new Label();
            _labelWeight.Text = "Weight, gr:"; /// удалила размерность
            _labelWeight.Parent = this;
            _labelWeight.Location = new Point(50, 140);

            _textBoxCalorificOfHundredGrams = new TextBox();
            _textBoxCalorificOfHundredGrams.Parent = this;
            _textBoxCalorificOfHundredGrams.Location = new Point(200, 110);
            _textBoxCalorificOfHundredGrams.Text = data.GetDataCalorificOfHundredGrams().ToString();
            _textBoxCalorificOfHundredGrams.KeyPress += _textBox_KeyPress;

            _textBoxWeight = new TextBox();
            _textBoxWeight.Parent = this;
            _textBoxWeight.Location = new Point(200, 140);
            _textBoxWeight.Text = data.GetDataWeight().ToString();
            _textBoxWeight.KeyPress += _textBox_KeyPress;

            _buttonCancel = new Button();
            _buttonCancel.Parent = this;
            _buttonCancel.Text = "Cancel";
            _buttonCancel.Location = new Point(110 + _buttonLength, Height - 80);
            _buttonCancel.Size = new Size(_buttonLength, _buttonWidth);
            _buttonCancel.Click += _buttonCancel_Click;

            _buttonOk = new Button();
            _buttonOk.Parent = this;
            _buttonOk.Text = "OK";
            _buttonOk.Location = new Point(100, Height - 80);
            _buttonOk.Size = new Size(_buttonLength, _buttonWidth);
            _buttonOk.Click += _buttonOk_Click;

            if (data.GetDataSort() == _sortCabbage)
            {
                _sort = _sortCabbage;

                _selectedTypeLabel = new Label();
                _selectedTypeLabel.Parent = this;
                _selectedTypeLabel.Location = new Point(200, 30);
                _selectedTypeLabel.Text = "Cabagge";

                _labelWeightOfInedibleLeaves = new Label();
                _labelWeightOfInedibleLeaves.Text = "WeightOfInedibleLeaves, gr:";  
                _labelWeightOfInedibleLeaves.Parent = this;
                _labelWeightOfInedibleLeaves.Location = new Point(50, 170);
                _labelWeightOfInedibleLeaves.AutoSize = true;

                _textBoxWeightOfInedibleLeaves = new TextBox();
                _textBoxWeightOfInedibleLeaves.Parent = this;
                _textBoxWeightOfInedibleLeaves.Location = new Point(200, 170);
                _textBoxWeightOfInedibleLeaves.Text = data.GetDataWeightOfInedibleLeaves().ToString();
                _textBoxWeightOfInedibleLeaves.KeyPress += _textBox_KeyPress;
            }

            if (data.GetDataSort() == _sortRoot)
            {
                _sort = _sortRoot;

                _selectedTypeLabel = new Label();
                _selectedTypeLabel.Parent = this;
                _selectedTypeLabel.Location = new Point(200, 30);
                _selectedTypeLabel.Text = "Root";

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight, gr:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);
                _textBoxPeelWeight.Text = data.GetDataPeelWeight().ToString();
                _textBoxPeelWeight.KeyPress += _textBox_KeyPress;

            }
            if (data.GetDataSort() == _sortCarrot)
            {
                _sort = _sortCarrot;

                _selectedTypeLabel = new Label();
                _selectedTypeLabel.Parent = this;
                _selectedTypeLabel.Location = new Point(200, 30);
                _selectedTypeLabel.Text = "Carrot";

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight, gr:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);
                _textBoxPeelWeight.Text = data.GetDataPeelWeight().ToString();
                _textBoxPeelWeight.KeyPress += _textBox_KeyPress;

                _labelPrepared = new Label();
                _labelPrepared.Text = "Prepared:";
                _labelPrepared.Parent = this;
                _labelPrepared.Location = new Point(50, 200);

                _textBoxPrepared = new TextBox();
                _textBoxPrepared.Parent = this;
                _textBoxPrepared.Location = new Point(200, 200);
                _textBoxPrepared.Text = data.GetDataPrepared();

                if (Equals(data.GetDataPrepared(), "-50"))
                    _textBoxPrepared.Text = "Boiled";
                if (Equals(data.GetDataPrepared(), "100"))
                    _textBoxPrepared.Text = "Fried";
                if (Equals(data.GetDataPrepared(), "0"))
                    _textBoxPrepared.Text = "Raw";
            }
        }
  
        private void _Clean()
        {
            if (_labelPeelWeight != null) _labelPeelWeight.Hide();
            if (_textBoxPeelWeight != null) _textBoxPeelWeight.Hide();

            if (_labelPrepared != null) _labelPrepared.Hide();
            if (_textBoxPrepared != null) _textBoxPrepared.Hide();

            if (_labelWeightOfInedibleLeaves != null) _labelWeightOfInedibleLeaves.Hide();
            if (_textBoxWeightOfInedibleLeaves != null) _textBoxWeightOfInedibleLeaves.Hide();

        }

        private void _radioButtonCabbage_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonCabbage.Checked)
            {
                _sort = _sortCabbage;
                _Clean();

                _labelWeightOfInedibleLeaves = new Label();
                _labelWeightOfInedibleLeaves.Text = "WeightOfInedibleLeaves, gr:";
                _labelWeightOfInedibleLeaves.Parent = this;
                _labelWeightOfInedibleLeaves.Location = new Point(50, 170);
                _labelWeightOfInedibleLeaves.AutoSize = true;

                _textBoxWeightOfInedibleLeaves = new TextBox();
                _textBoxWeightOfInedibleLeaves.Parent = this;
                _textBoxWeightOfInedibleLeaves.Location = new Point(200, 170);
                _textBoxWeightOfInedibleLeaves.KeyPress += _textBox_KeyPress;
            }
        }

        private void _radioButtonRoot_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonRoot.Checked)
            {
                _sort = _sortRoot;
                _Clean();

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight, gr:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);
                _textBoxPeelWeight.KeyPress += _textBox_KeyPress;
            }
        }

        private void _radioButtonCarrot_CheckedChanged(object sender, EventArgs e)
        {
            if (_radioButtonCarrot.Checked)
            {
                _sort = _sortCarrot;
                _Clean();

                _labelPeelWeight = new Label();
                _labelPeelWeight.Text = "PeelWeight, gr:";
                _labelPeelWeight.Parent = this;
                _labelPeelWeight.Location = new Point(50, 170);

                _textBoxPeelWeight = new TextBox();
                _textBoxPeelWeight.Parent = this;
                _textBoxPeelWeight.Location = new Point(200, 170);
                _textBoxPeelWeight.KeyPress += _textBox_KeyPress;

                _labelPrepared = new Label();
                _labelPrepared.Text = "Prepared:";
                _labelPrepared.Parent = this;
                _labelPrepared.Location = new Point(50, 200);                

                _textBoxPrepared = new TextBox();
                _textBoxPrepared.Parent = this;
                _textBoxPrepared.Location = new Point(200, 200);
            }
        }

        private void _buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _buttonOk_Click(object sender, EventArgs e)
        {

            if (_radioButtonCabbage != null && !_radioButtonCabbage.Checked && _radioButtonCarrot != null &&
                    !_radioButtonCarrot.Checked && _radioButtonRoot != null && !_radioButtonRoot.Checked)
                MessageBox.Show("You need to choose the type of vegetable!");
            else
            {
                bool emptyTextboxName = false;
                bool emptyTextboxWeight = false;
                bool emptyTextboxCalorificOfHundredGrams = false;
                bool emptyTextboxWeightOfInedibleLeaves = false;
                bool emptyTextboxPeelWeight = false;
                bool emptyTextboxPrepared = false;
                bool correctlyFilledTextboxPrepared = false;

                if (_textBoxName != null)
                    emptyTextboxName = _textBoxName.Text == "";
                if (_textBoxWeight != null)
                    emptyTextboxWeight = _textBoxWeight.Text == "";
                if (_textBoxCalorificOfHundredGrams != null)
                    emptyTextboxCalorificOfHundredGrams = _textBoxCalorificOfHundredGrams.Text == "";
                if (_textBoxWeightOfInedibleLeaves != null)
                    emptyTextboxWeightOfInedibleLeaves = _textBoxWeightOfInedibleLeaves.Text == "";
                if (_textBoxPeelWeight != null)
                    emptyTextboxPeelWeight = _textBoxPeelWeight.Text == "";
                if (_textBoxPrepared != null)
                    emptyTextboxPrepared = _textBoxPrepared.Text == "";
                if (_textBoxPrepared != null)
                    correctlyFilledTextboxPrepared = _textBoxPrepared.Text == "Boiled" || _textBoxPrepared.Text == "Fried" || _textBoxPrepared.Text == "Raw";

                    if (_sort == _sortCabbage)
                    if (emptyTextboxName || emptyTextboxWeight || emptyTextboxCalorificOfHundredGrams || emptyTextboxWeightOfInedibleLeaves)
                        Alert();
                    else
                    {
                        if (Int32.Parse(_textBoxWeightOfInedibleLeaves.Text) >= Double.Parse(_textBoxWeight.Text))
                            MessageBox.Show("The weight of inedible leaves cannot be greater than or equal to the weight of cabbage!");
                        else
                        {
                            data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                            Int32.Parse(_textBoxWeightOfInedibleLeaves.Text));
                            Finisher();
                        }
                    }
                if (_sort == _sortRoot)
                    if (emptyTextboxName || emptyTextboxWeight || emptyTextboxCalorificOfHundredGrams || emptyTextboxPeelWeight)
                        Alert();
                    else
                    {
                        if (Double.Parse(_textBoxPeelWeight.Text) >= Double.Parse(_textBoxWeight.Text))
                            MessageBox.Show("The weight of the peel cannot be greater than or equal to the weight of the root!");
                        else
                        {
                            data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                                                      Double.Parse(_textBoxPeelWeight.Text));
                            Finisher();
                        }                       
                    }

                if (_sort == _sortCarrot)
                    if (emptyTextboxName || emptyTextboxWeight || emptyTextboxCalorificOfHundredGrams || emptyTextboxPeelWeight || emptyTextboxPrepared)
                        Alert();
                    else
                    {
                        if (!correctlyFilledTextboxPrepared)
                            MessageBox.Show("The cooking type is not selected correctly. You need to enter Boiled , Fried or Raw !");
                        else
                        {
                            if (Double.Parse(_textBoxPeelWeight.Text) >= Double.Parse(_textBoxWeight.Text))
                                MessageBox.Show("The weight of the peel cannot be greater than or equal to the weight of the carrot!");
                            else
                            {
                                data = new Data(_textBoxName.Text, Double.Parse(_textBoxWeight.Text), Double.Parse(_textBoxCalorificOfHundredGrams.Text),
                                Double.Parse(_textBoxPeelWeight.Text), _textBoxPrepared.Text);
                                Finisher();
                            }
                        }
                    }
            }
        }

        public void Finisher()
        {
            data.MoveToFile();
            Close();
            if (_windowType == _windowTypeAdd)
            {
                OKButtonClickedAdd();
            }
            if (_windowType == _windowTypeEdit)
            {
                OKButtonClickedEdit();
            }
        }

        public void Alert()
        {
           MessageBox.Show("All fields must be filled in!");  
        }
    }
}
