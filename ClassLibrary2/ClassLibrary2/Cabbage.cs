﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vegetables
{
    public class Cabbage : Vegetable
    {
        private int _weightOfInedibleLeaves = 0;
        public Cabbage()
        {
            vegetableType = 1;
        }
        public int WeightOfInedibleLeaves
        {
            get { return _weightOfInedibleLeaves; }
            set
            {
                _weightOfInedibleLeaves = value;
                Weight = Weight - _weightOfInedibleLeaves;
            }
        }
    }
}
