﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vegetables;

namespace WindowsFormsApplication1
{
    class AddEditDelete
    {
        public static void ReplaceInSalad(Vegetable vegetable, Salad salad, int index)
        {
            salad.DeleteVegetable(index);
            salad.AddVegetable(index, vegetable);
        }

        public static void ReplaceInList(Vegetable vegetable, ListBox listbox, int index)
        {
            listbox.Items.RemoveAt(index);
            listbox.Items.Insert(index, vegetable.GetVegetableInfo());
        }

        public static void AddToList(Vegetable vegetable, ListBox listbox, int index)
        {
            listbox.Items.Insert(index, vegetable.GetVegetableInfo());
        }

        public static void AddToSalad(Vegetable vegetable, Salad salad)
        {
            salad.AddVegetable(vegetable);
        }

        public static void DeleteFromList(ListBox listbox, int index)
        {
            listbox.Items.RemoveAt(index);
        }

        public static void DeleteFromSalad(Salad salad, int index)
        {
            salad.DeleteVegetable(index);
        }
    }
}

